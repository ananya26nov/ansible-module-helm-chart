# ansible-module-helm-chart

Ansible module to manipulate values in helm charts

```
DOCUMENTATION = '''
---
module: helm_chart
short_description: Adds/updates custom registry url and registry pull secret to helm chart
'''

EXAMPLES = '''
- name: Update helm chart
      helm_chart:
        path: "/Users/somepath/extracted_chart_directory"
        registry_url: "Myregistry"
        registry_pullSecret: "MyPullSecret"

```      