#!/usr/bin/python

DOCUMENTATION = '''
---
module: helm_chart
short_description: Adds/updates custom registry url and registry pull secret to helm chart
'''

EXAMPLES = '''
- name: Update helm chart
      helm_chart:
        path: "/Users/somepath/extracted_chart_directory"
        registry_url: "Myregistry"
        registry_pullSecret: "MyPullSecret"
      register: result
'''

from ansible.module_utils.basic import *
import yaml


def list_all_matched_files(file_path, file_name):
	"""
	List all file paths that match given filename recursively in given path
	:param file_path: String file path
	:param file_name:  String file name
	:return: list of files matched
	"""
	files_matched = []
	for dirpath, dir_names, file_names in os.walk(file_path):
		for filename in [f for f in file_names if f == file_name]:
			file_found = os.path.join(dirpath, filename)
			files_matched.append(file_found)
	return files_matched


def update_values_files(file_path, registry_url, registry_pullSecret):
	"""
	Updated registry url and pull secret values  under global and image
	credentials section
	:param file_path: String path of extracted helm chart
	:param registry_url: String new docker registry url
	:param registry_pullSecret: String name of k8  pull secret
	:return: has_changed and meta
	"""
	file_name = 'values.yaml'
	files_matched = list_all_matched_files(file_path, file_name)
	for values_file in files_matched:
		with open(values_file, 'r') as stream:
			values_yaml = yaml.load(stream)
		if 'global' in values_yaml:
			if 'registry' in values_yaml['global']:
				values_yaml['global']['registry']['url'] = registry_url
				values_yaml['global']['registry']['pullSecret'] = registry_pullSecret
			else:
				values_yaml['global']['registry'] = {}
				values_yaml['global']['registry']['url'] = registry_url
				values_yaml['global']['registry']['pullSecret'] = registry_pullSecret
		else:
			values_yaml['global'] = {}
			values_yaml['global']['registry'] = {}
			values_yaml['global']['pullSecret'] = {}
			values_yaml['global']['registry']['url'] = registry_url
			values_yaml['global']['pullSecret']['url'] = registry_pullSecret

		if 'imageCredentials' in values_yaml:
			if 'registry' in values_yaml['imageCredentials']:
				values_yaml['imageCredentials']['registry']['url'] = registry_url
				values_yaml['imageCredentials']['registry']['pullSecret'] = registry_pullSecret
			else:
				values_yaml['imageCredentials']['registry'] = {
					"url": registry_url,
					"pullSecret": registry_pullSecret
				}
		else:
			values_yaml['imageCredentials'] = {
				"registry": {
					"url": registry_url,
					"pullSecret": registry_pullSecret
				}
			}

		with open(values_file, 'w') as outfile:
			yaml.dump(values_yaml, outfile)

	has_changed = True
	meta = {"file_path": file_path, "registry_url": registry_url}
	return has_changed, meta


def main():
	fields = {
		"path": {"required": True, "type": "str"},
		"registry_url": {"required": False, "type": "str"},
		"registry_pullSecret": {"required": False, "type": "str"}
	}
	has_changed = False
	result = []
	module = AnsibleModule(argument_spec=fields)
	if module.params['registry_url'] or \
	module.params['registry_url'] or \
	module.params['registry_pullSecret'] or \
	module.params['update_repoPath']:
		task_has_changed, task_result = update_values_files(
			module.params['path'],
			module.params['registry_url'],
			module.params['registry_pullSecret'],)
		has_changed = has_changed or task_has_changed
		result.append(task_result)

	module.exit_json(changed=has_changed, meta=result)


if __name__ == '__main__':
	main()
